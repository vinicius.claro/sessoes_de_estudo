Repositório de guias de sessões de estudo referentes ao módulo I do curso - Champions.

Estão nesse repositório as seguintes 6 sessões:
01 - Versionamento 
02 - Orientação a Objetos
03 - Java I - Sintaxe da linguagem
04 - Java II - Encapsulamento
05 - Java III - Herança
06 - Java IV - Polimorfismo

Além das sessões temos o Guia do Multiplicador, que propõe uma dinâmica de mentoria, uma sequência proposta de estudos e algumas interações em formatos diferentes.

